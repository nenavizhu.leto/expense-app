import './App.css';
import CategoryList from './components/CategoryList/CategoryList';

const categories = [
	{ title: 'Groceries', color: 'blue' },
	{ title: 'Restaurant', color: 'purple' },
	{ title: 'Leisure', color: 'red' },
	{ title: 'Transport', color: 'yellow' },
]
function App() {
	return (
		<div className="App">
			<CategoryList items={categories} />
		</div>
	);
}

export default App;
