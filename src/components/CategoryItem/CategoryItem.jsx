import React from 'react'
import './CategoryItem.css'

const CategoryItem = ({ title, color, Icon }) => {
	console.log(title, color)
	return (
		<div className='category-item'>
			<div className="category-item__title">{title}</div>
			<div className='category-item__icon' style={{ backgroundColor: color }}>
			</div>
			<div className='category-item__amount'>
				$ 0
			</div>
		</div>
	)
}

export default CategoryItem
