import React from 'react'
import './CategoryList.css'
import CategoryItem from '../CategoryItem/CategoryItem.jsx'

const CategoryList = ({ items }) => {
	// TODO: Maybe choose color from predefined ones
	return (
		<ul className='category-list'>
			{items.map((item, i) => (
				<li key={i} className='category-list__item'>
					<CategoryItem title={item.title} color={item.color} />
				</li>
			))}
		</ul>
	)
}

export default CategoryList
